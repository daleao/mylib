#pragma once

#include <iostream>

/*
 * Simple template for printing any number of variables to the console.
 */

namespace ml {

    void print() { std::cout << std::endl; }
    
    template <typename head_t, typename... tail_t>
    void print(const head_t &head, const tail_t &... tail) {
      std::cout << head << " ";
      print(tail...);
    }

} // namespace ml
