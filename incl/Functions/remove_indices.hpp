#pragma once

/*
 * Removes specified index elements from a container.
 * Indices should be supplied as a vector of integers.
 */

#include <algorithm>
#include <vector>

namespace ml {

	template <typename Container>
	void remove_indices(Container& container, const std::vector<int>& indices_to_remove) {
		auto container_base = container.begin();
		typename Container::size_type down_by = 0;
		for (auto it = indices_to_remove.cbegin(), too_far = indices_to_remove.cend(); it != too_far; ++it, ++down_by) {
			typename Container::size_type next = it + 1 == indices_to_remove.cend() ? container.size() : *(it + 1);
			std::move(container_base + *it + 1, container_base + next, container_base + *it - down_by);
		}
		container.resize(container.size() - indices_to_remove.size());
	}

} // namespace ml