#pragma once

/*
 * Flips an associative container of A,B pairs to B,A pairs.
 */

#include <algorithm>
#include <iterator>
#include <map>

namespace ml {

	template<typename A, typename B>
	std::pair<B, A> flip_pair(const std::pair<A, B>& p) {
		return std::pair<B, A>(p.second, p.first);
	}

	template<typename A, typename B, template<class, class, class...> class M, class... Args>
	std::multimap<B, A> flip_map(const M<A, B, Args...>& src)
	{
		std::multimap<B, A> dst;
		std::transform(src.begin(), src.end(),
			std::inserter(dst, dst.begin()),
			flip_pair<A, B>);
		return dst;
	}

} // namespace ml