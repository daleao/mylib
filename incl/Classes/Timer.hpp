#pragma once

/*
 * Simple timer class for benchmarking.
 * Elapsed time is returned in any specified unit.
 * Alternatively returns elapsed time as duration to facilitate time-based
 * arithmetics with the chrono library.
 */

#include <chrono>

namespace ml {

    template <typename clock_t = std::chrono::_V2::high_resolution_clock>
    class Timer {
    private:
      typename clock_t::time_point _start;
    
    public:
      // Constructor/Start timer
      Timer(void);
    
      // Recover elapsed time count in 'unit_t' units
      template <typename unit_t = std::chrono::microseconds>
      typename unit_t::rep elapsed(void) const;
    
      // Recover elasped time as duration
      typename clock_t::duration elapsed(void) const;
    
      // Reset timer
      void reset(void);
    };

} // namespace ml

#include "Timer.ipp"
