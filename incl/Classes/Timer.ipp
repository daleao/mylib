#pragma once

// Constructor/Start timer
template <typename clock_t>
ml::Timer<clock_t>::Timer(void) : _start(clock_t::now()) {}

// Recover elapsed time count in 'unit_t' units
template <typename clock_t>
template <typename unit_t>
typename unit_t::rep ml::Timer<clock_t>::elapsed(void) const {
  auto _elapsed = std::chrono::duration_cast<unit_t>(clock_t::now() - _start);
  return static_cast<typename unit_t::rep>(_elapsed.count());
}

// Recover elapsed time as duration
template <typename clock_t>
typename clock_t::duration ml::Timer<clock_t>::elapsed(void) const {
  return (clock_t::now() - _start);
}

// Reset timer
template <typename clock_t> void ml::Timer<clock_t>::reset(void) {
  _start = clock_t::now();
}